﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArrayGesamtUmsatz.Exeptions
{
	class NoSavedRevenuesExeption : Exception
	{
		public NoSavedRevenuesExeption(string message) : base(message)
		{
		}
	}
}
