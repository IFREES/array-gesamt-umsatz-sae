﻿using ArrayGesamtUmsatz.Exeptions;
using ArrayGesamtUmsatz.Models;
using ArrayGesamtUmsatz.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace ArrayGesamtUmsatz.ViewModel
{
	/// <summary>
	/// View Model for MainWindow
	/// </summary>
	public class RevenueViewModel : BaseViewModel
    {
		/// <summary>
		/// Private variables for Properties
		/// </summary>
		private int _currentRevenueIndex;
		private Decimal _revenueUserInput = Decimal.Zero;
		private RevenueModel _revenueObject = new RevenueModel(12);

		/// <summary>
		/// Gets and sets the _currentRevenueIndex
		/// </summary>
		public int CurrentRevenueIndex
		{
			get { return _currentRevenueIndex; }
			set
			{
				_currentRevenueIndex = value;
				NotifyPropertyChanged(nameof(CurrentRevenueIndex));
				NotifyPropertyChanged(nameof(PossitionText));
			}
		}
		/// <summary>
		/// Gets and sets the _revenueUserInput
		/// </summary>
		public Decimal RevenuUserInput
		{
			get
			{
				if (_revenueUserInput == Decimal.Zero)
				{
					_revenueUserInput = _revenueObject.RevenueList[CurrentRevenueIndex];
				}
				return _revenueUserInput;
			}
			set
			{
				_revenueUserInput = value;
				NotifyPropertyChanged(nameof(RevenuUserInput));
				NotifyPropertyChanged(nameof(AllEnteredRevenuesText));
			}
		}

		/// <summary>
		/// Gets the possition text for the UI
		/// </summary>
		public string PossitionText
		{
			get
			{
				return $"Umsatz {CurrentRevenueIndex + 1} von {_revenueObject.RevenueList.Count}";
			}
		}
		/// <summary>
		/// Gets the text for displaying all entered revenues for the UI
		/// </summary>
		public string AllEnteredRevenuesText
		{
			get
			{
				List<Decimal> savedRevenues = _revenueObject.SavedRevenues;
				string result = $"Bisher eingegebene Umsätze ({savedRevenues.Count}): ";

				savedRevenues.ForEach((revenue) =>
				{
					int index = savedRevenues.IndexOf(revenue);
					if (index + 1 == savedRevenues.Count)
						result += revenue;
					else
						result += $"{revenue} - ";
				});
				return result.Replace('.', ',');
			}
		}
		/// <summary>
		/// Gets and the avrage revenue text for the UI
		/// </summary>
		public string AvrageRevenueText
		{
			get
			{
				try
				{
					Decimal avrageRevenue = _revenueObject.AvrageRevenue;
					return Decimal.Round(_revenueObject.AvrageRevenue, 2, MidpointRounding.ToEven).ToString();
				}
				catch (NoSavedRevenuesExeption)
				{
					return "Noch keine Umsätze angegeben";
				}
			} 
			
		}
		/// <summary>
		/// Gets the highest saved revenue for the UI
		/// </summary>
		public string MaxRevenueText
		{
			get
			{
				try
				{
					Decimal maxRevenue = _revenueObject.MaxRevenue;
					return maxRevenue.ToString();
				}
				catch (NoSavedRevenuesExeption)
				{
					return "Noch keine Umsätze angegeben";
				}
				
			}

		}
		/// <summary>
		/// Gets the lowest saved revenue for the UI
		/// </summary>
		public string MinRevenueText
		{
			get
			{
				try
				{
					Decimal minRevenue = _revenueObject.MinRevenue;
					return minRevenue.ToString();
				}
				catch (NoSavedRevenuesExeption)
				{
					return "Noch keine Umsätze angegeben";
				}
			}
			
		}

		/// <summary>
		/// Gets and sets the buttons for the UI 
		/// </summary>
		public ICommand BackButtonCommand { get; set; }
		public ICommand NextButtonCommand { get; set; }
		public ICommand SaveButtonCommand { get; set; }

		/// <summary>
		/// Sets all the buttons to a relay command
		/// </summary>
		public RevenueViewModel()
		{
			BackButtonCommand = new RelayCommand(ExecuteBackButton, CanExecuteBackButton);
			NextButtonCommand = new RelayCommand(ExecuteNextButton, CanExecuteNextButton);
			SaveButtonCommand = new RelayCommand(ExecuteSaveButton, CanExecuteSaveButton);
		}

		/// <summary>
		/// Cecks if the buttons can be executed
		/// </summary>
		/// <param name="parameter"></param>
		/// <returns></returns>
		private bool CanExecuteBackButton(object parameter) => CurrentRevenueIndex > 0;
		private bool CanExecuteNextButton(object parameter) => CurrentRevenueIndex < (_revenueObject.RevenueList.Count - 1);
		private bool CanExecuteSaveButton(object parameter) => RevenuUserInput != Decimal.Zero;

		/// <summary>
		/// The execution functions for the buttons
		/// </summary>
		/// <param name="parameter"></param>
		private void ExecuteBackButton(object parameter)
		{
			CurrentRevenueIndex -= 1;
			RevenuUserInput = Decimal.Zero;
		}
		private void ExecuteNextButton(object parameter)
		{
			CurrentRevenueIndex += 1;
			RevenuUserInput = Decimal.Zero;
		}
		private void ExecuteSaveButton(object parameter)
		{
			_revenueObject.RevenueList[CurrentRevenueIndex] = _revenueUserInput;
			NotifyPropertyChanged(nameof(AllEnteredRevenuesText));
			NotifyPropertyChanged(nameof(AvrageRevenueText));
			NotifyPropertyChanged(nameof(MinRevenueText));
			NotifyPropertyChanged(nameof(MaxRevenueText));
		}

	}
}