﻿using ArrayGesamtUmsatz.Exeptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ArrayGesamtUmsatz.Models
{
	public class RevenueModel
	{
		/// <summary>
		/// Gets and sets the List that holdes all the saved revenues
		/// </summary>
		public List<Decimal> RevenueList { get; set; }

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="revenuesCount"></param>
		public RevenueModel(int revenuesCount)
		{
			RevenueList = new List<decimal>(new Decimal[revenuesCount]);
		}

		/// <summary>
		/// Gets all saved revenues
		/// </summary>
		public List<Decimal> SavedRevenues
		{
			get => RevenueList.Where(revenue => revenue != 0).ToList();
		}

		/// <summary>
		/// Calculates the avrage value
		/// </summary>
		/// <returns></returns>
		public Decimal AvrageRevenue
		{
			get
			{
				decimal totalSum = Decimal.Zero;
				int countOfValidEntries = 0;
				RevenueList.ForEach(revenue =>
				{
					if (revenue != Decimal.Zero)
					{
						totalSum += revenue;
						countOfValidEntries += 1;
					}
				});
				return countOfValidEntries == 0 ? throw new NoSavedRevenuesExeption("No Saved Revenues") : Decimal.Divide(totalSum, countOfValidEntries);
			}
		}

		/// <summary>
		/// Calculates the max value
		/// </summary>
		/// <returns></returns>
		public Decimal MaxRevenue
		{
			get => SavedRevenues.Count == 0 ? throw new NoSavedRevenuesExeption("No Saved Revenues") : SavedRevenues.Max();
		}

		/// <summary>
		/// Calculates the min value
		/// </summary>
		/// <returns></returns>
		public Decimal MinRevenue
		{
			get => SavedRevenues.Count == 0 ? throw new NoSavedRevenuesExeption("No Saved Revenues") : SavedRevenues.Min();
		}
	}
}
